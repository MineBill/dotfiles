vim.opt.hidden = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.showcmd = true

vim.opt.signcolumn = "yes"
vim.opt.foldlevel = 100
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()" -- TODO: ???

vim.opt.inccommand = "nosplit"
vim.opt.incsearch = true
vim.opt.title = true
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.wb = false

vim.opt.autoread = true
vim.opt.undofile = true
vim.opt.undodir = os.getenv("HOME") .. "/.vimundo"

vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.tabstop = 4 -- FIXME: ^^
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4 -- FIXME: Look what this actually is
vim.opt.expandtab = true
vim.opt.termguicolors = true
vim.opt.colorcolumn = "140"
vim.opt.laststatus = 3
vim.opt.fillchars = {
	horiz = "━",
	horizup = "┻",
	horizdown = "┳",
	vert = "┃",
	vertleft = "┫",
	vertright = "┣",
	verthoriz = "╋",
}
vim.opt.list = true
vim.opt.showbreak="↪\\ "
vim.opt.listchars="tab:  ,nbsp:␣,trail:•,extends:⟩,precedes:⟨"
vim.opt.scrolloff = 5

vim.opt.wrap = false
vim.opt.linebreak = true
vim.opt.cmdheight = 1
vim.cmd([[set clipboard+=unnamedplus]])
vim.cmd([[set completeopt=menuone]])
-- vim.opt.completeopt = "menuone"

vim.g.zig_fmt_autosave = 0

vim.cmd([[
    if has("vim_starting")
        set runtimepath += "~/.config/nvim"
    endif
]])

vim.cmd([[
    if has('mouse')
        set mouse=nv
        set mousemodel=popup
    endif
    set guicursor=n-v-c:block-Cursor,i-ci-ve:ver25-Cursor,r-cr-o:hor20-Cursor
]])

vim.cmd([[set winbar=%=%m\ %f]])

vim.g.catppuccin_flavour = "latte"

vim.cmd([[colorscheme base16-gruvbox-dark-hard]])

vim.cmd([[hi! Cursor guifg=NONE guibg=#ff4040]])
vim.g.neovide_refresh_rate = 144
vim.g.neovide_transparency = 1.0
vim.g.neovide_floating_opacity = 0.95
vim.g.neovide_cursor_animation_length = 0.05
vim.g.neovide_scroll_animation_length = 0.5
vim.g.neovide_cursor_vfx_mode = "railgun"
vim.g.neovide_cursor_vfx_particle_density = 50.0
vim.g.neovide_cursor_vfx_particle_phase = 10.0
vim.g.neovide_cursor_vfx_particle_limetime = 1.0
vim.g.neovide_cursor_vfx_particle_speed = 20

function map(mode, shortcut, command)
	vim.api.nvim_set_keymap(mode, shortcut, command, {
		noremap = true,
		silent = true,
	})
end

function nmap(shortcut, command)
	map("n", shortcut, command)
end

function vmap(shortcut, command)
	map("v", shortcut, command)
end

function imap(shortcut, command)
	map("i", shortcut, command)
end

function tmap(shortcut, command)
	map("t", shortcut, command)
end

vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

imap("fd", "<Esc>")
imap("<C-k>", "<Up>")
imap("<C-j>", "<Down>")

nmap("J", "<Nop>")
nmap("K", "<Nop>")

nmap("<C-h>", "<C-W>h")
nmap("<C-l>", "<C-W>l")
nmap("<C-j>", "<C-W>j")
nmap("<C-k>", "<C-W>k")

nmap("<S-j>", ':<C-u>execute "keepjumps norm! " . v:count1 . "}"<CR>')
nmap("<S-k>", ':<C-u>execute "keepjumps norm! " . v:count1 . "{"<CR>')
nmap("<S-l>", ":bn<CR>")
nmap("<S-h>", ":bp<CR>")

vmap("<C-J>", "<Down>")
vmap("<C-K>", "<Up>")
vmap("J", "<Nop>")
vmap("K", "<Nop>")

nmap("<Leader>fed", ":e $MYVIMRC<CR>")
nmap("<Leader>occ", ":e ~/.config/nvim/lua/cmp_config.lua<CR>")
nmap("<Leader>ocp", ":e ~/.config/nvim/lua/plugins.lua<CR>")
nmap("<Leader>ocl", ":e ~/.config/nvim/lua/lsp/config.lua<CR>")
nmap("<Leader>ocd", ":e ~/.config/nvim/lua/debugging.lua<CR>")

nmap("<C-n>", ":noh<CR>")
nmap("n", "nzz")
nmap("N", "Nzz")
-- nmap("<C-p>", "<C-o>")
nmap("<C-p>", ":Telescope find_files<CR>")
tmap("<C-p>", "<C-c>")
nmap("<C-f>", ":Telescope live_grep<CR>")
nmap("<C-b>", ":Telescope buffers<CR>")
nmap("<Leader>ca", ":lua require('telescope.builtin').lsp_code_actions(require('telescope.themes').get_cursor())<CR>")
nmap("<Leader>q", ":Bd<CR>")

-- Terminal commands

vim.g.floaterm_shell = "/usr/bin/fish"
vim.g.floaterm_wintype = "float"
vim.g.floaterm_position = "bottom"
vim.g.floaterm_width = 0.9
vim.g.floaterm_height = 0.5


tmap("<Esc>","<C-\\><C-n>")
nmap("<C-n>", ":FloatermToggle<CR>")
tmap("<C-n>", "<C-\\><C-n>:FloatermToggle<CR>")

vim.cmd([[highlight! CmpItemAbbrMatch guibg=NONE guifg=#569CD6]])
vim.cmd([[highlight! cmpitemabbrmatchfuzzy guibg=none guifg=#569cd6]])
vim.cmd([[highlight! cmpitemkindfunction guibg=none guifg=#c586c0]])
vim.cmd([[highlight! cmpitemkindmethod guibg=none guifg=#c586c0]])
vim.cmd([[highlight! cmpitemkindvariable guibg=none guifg=#9cdcfe]])
vim.cmd([[highlight! cmpitemkindkeyword guibg=none guifg=#d4d4d4]])

vim.cmd([[autocmd BufEnter *.zig set ft=zig]])
vim.cmd([[autocmd BufEnter *.odin set ft=odin]])
-- vim.cmd([[set guifont=SauceCodePro\ Nerd\ Font\ Mono:h11]])
vim.cmd([[set guifont=JetBrains\ Mono:h12]])

require("plugins")
require("lsp/config")
require("cmp_config")

local colors = vim.fn.getcompletion('', 'color')
function NextColor()
	local index = vim.fn.index(colors, vim.g.colors_name)
	local name = colors[index + 2]
    vim.cmd("colorscheme " .. name)
end

function PreviousColor()
	local index = vim.fn.index(colors, vim.g.colors_name)
	local name = colors[index]
    vim.cmd("colorscheme " .. name)
end

nmap("<Leader>n", ":lua NextColor()<CR>")
nmap("<Leader>p", ":lua PreviousColor()<CR>")
