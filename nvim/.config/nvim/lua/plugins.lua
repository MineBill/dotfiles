function plugin_config()
    require("lspconfig")

    local kanagawa = require("kanagawa")
    kanagawa.setup({
        undercurl = true,           -- enable undercurls
        commentStyle = { italic = true },
        functionStyle = { bold = true },
        keywordStyle = { bold = true },
        statementStyle = { bold = true },
        typeStyle = {},
        variablebuiltinStyle = {},
        specialReturn = true,       -- special highlight for the return keyword
        specialException = true,    -- special highlight for exception handling keywords
        transparent = false,        -- do not set background color
        dimInactive = true,        -- dim inactive window `:h hl-NormalNC`
        globalStatus = false,       -- adjust window separators highlight for laststatus=3
        terminalColors = true,      -- define vim.g.terminal_color_{0,17}
        colors = {},
        overrides = {},
        theme = "default"           -- Load "default" theme or the experimental "light" theme
    })
    -- vim.cmd("colorscheme kanagawa")

    local colorizer = require("colorizer")
    colorizer.setup()

    local lualine = require("lualine")
    lualine.setup({
        theme = "gruvbox-flat",
        options = {
            component_separators = "",
            section_separators = "",
            globalstatus = true,
        },
        lualine = {
            lualine_a = {},
            lualine_b = { "branch" },
            lualine_c = { "filename" },
            lualine_x = { "branch" },
            lualine_y = {},
            lualine_z = {},
        },
    })

    local bufferline = require("bufferline")
    local color = "#101010"
    bufferline.setup({
        options = {
            separator_style = "slant",
            diagnostics = "nvim_lsp",
            offsets = {
                {
                    filetype = "NvimTree",
                    text = "File Explorer",
                },
            },
        },
        highlights = {
            fill = {
                bg = color,
            },
            separator_selected = {
                fg = color,
            },
            separator_visible = {
                fg = color,
            },
            separator = {
                fg = color,
            },
        },
    })

    local treesitter_configs = require("nvim-treesitter.configs")
    treesitter_configs.setup({
        highlight = {
            enable = true,
            use_languagetree = true,
            additional_vim_regex_highlighting = false,
        },
        incremental_selection = {
            enable = true,
            keymaps = {
                init_selection = ";",
                node_incremental = ";",
                scope_incremental = "grc",
                node_decremental = ":",
            },
        },
        rainbow = {
            enable = true,
            colors = {
                "#ff5555",
                "#ffb86c",
                "#ff79c6",
                "#50fa7b",
                "#f1fa8c",
                "#8be9fd",
                "#458588",
            },
        },
    })

    local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
    parser_config.odin = {
      install_info = {
        url = os.getenv("HOME") .. "/git/treesitter-grammars/minebill-odin",
        files = {"src/parser.c"}
      },
      filetype = "odin",
    }

    local devicons = require("nvim-web-devicons")
    devicons.setup()

    local kommentary = require("kommentary.config")
    kommentary.use_extended_mappings()
    kommentary.configure_language("zig", {
        single_line_comment_string = "//",
        multi_line_comment_strings = false,
    })

    local telescope_actions = require("telescope.actions")
    local telescope = require("telescope")
    telescope.setup({
        defaults = {
            mappings = {
                i = {
                    ["<C-j>"] = telescope_actions.move_selection_next,
                    ["<C-k>"] = telescope_actions.move_selection_previous,
                },
            },
        },
    })

    local dap = require("dap")
    dap.adapters = {
        cpp = {
            type = "executable",
            command = "/usr/bin/lldb-vscode",
            name = "lldb",
        },
    }
    dap.configurations = {
        cpp = {
            {
                name = "Launch",
                type = "lldb",
                request = "launch",
                program = function()
                    return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
                end,
                cwd = "${workspaceFolder}",
                stopOnEntry = false,
                args = {},
                runInTerminal = false,
            },
        },
    }

    local cmake = require("cmake")
    cmake.setup({
        parameters_file = ".nvim.json",
        configure_args = {},
        build_dir = "build",
        dap_configuration = { type = "cpp", request = "launch" },
    })

    local pairs = require("pairs")
    pairs:setup()

    local nvimtree = require("nvim-tree")
    nvimtree.setup()

    local vstask = require("vstask")
    vstask.setup({})

    local blanklines = require("indent_blankline")
    blanklines.setup({
        char = "",
        context_char = "│",
        show_current_context = true,
        show_end_of_line = true,
    })

    local gitsigns = require("gitsigns")
    gitsigns.setup({})
end

local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd [[packadd packer.nvim]]
end

return require("packer").startup(function(use)
    -- The package manager
    use({ "wbthomason/packer.nvim" })

    -- Various LSP configurations
    use({ "neovim/nvim-lspconfig" })

    -- TreeSitter, used for highlighting
    use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })
    use({ "nvim-treesitter/playground" })

    -- Display current mode, buffer, lines etc.
    use({ "hoob3rt/lualine.nvim" })

    -- Completion framework
    use({ "hrsh7th/nvim-cmp" })
    use({ "hrsh7th/cmp-nvim-lsp" })
    use({ "hrsh7th/cmp-buffer" })
    use({ "hrsh7th/cmp-path" }) -- Paths
    use({ "hrsh7th/cmp-cmdline" }) -- vim's commandline (':')
    use({ "hrsh7th/cmp-nvim-lua" }) -- Lua configuration

    -- Automatically creates missing LSP diagnostics highlight groups for color schemes that don't yet support the Neovim 0.5 builtin lsp client.
    use({ "folke/lsp-colors.nvim" })

    -- Icons for autocompletion entries
    use({ "kyazdani42/nvim-web-devicons" })

    -- Used for selecting buffers and greping for file contents
    use({
        "nvim-telescope/telescope.nvim",
        requires = { { "nvim-lua/plenary.nvim" } },
    })

    use({"junegunn/fzf", run = "fzf#install()"})

    -- Used for inserting comments on any language
    use({ "b3nj5m1n/kommentary" })

    -- Delete a buffer but keep the layout
    use({ "famiu/bufdelete.nvim" })

    -- Show indentation even on empty lines
    use({ "lukas-reineke/indent-blankline.nvim" })
    use({
        "lewis6991/gitsigns.nvim",
        requires = {
            "nvim-lua/plenary.nvim",
        },
    })

    -- Debug Adapter Protocol client implementation for Neovim
    use({ "mfussenegger/nvim-dap" })

    -- CMake integration for Neovim
    use({ "Shatur/neovim-cmake" })

    -- Basically fake and prettier tabs
    use({ "akinsho/bufferline.nvim" })

    -- Auto complete (, [, ', ", `
    use({ "ZhiyuanLck/smart-pairs" })

    -- Random
    use({ "yamatsum/nvim-cursorline" })
    use({ "jose-elias-alvarez/null-ls.nvim" })
    use({ "chentau/marks.nvim" })
    use({"norcalli/nvim-colorizer.lua"})

    -- Project structure
    use({ "EthanJWright/vs-tasks.nvim" })
    use({ "pianocomposer321/yabs.nvim" })
    use({ "kyazdani42/nvim-tree.lua" })

    -- Language Support
    -- TODO: Install nushell stuff
    use({"Tetralux/odin.vim"})
    use({"jdonaldson/vaxe"})
    use({"Hoffs/omnisharp-extended-lsp.nvim"})
    use({"habamax/vim-godot"})

    -- Themes
    use({"tpope/vim-sleuth"})
    use({"voldikss/vim-floaterm"})
    use({"rebelot/kanagawa.nvim"})
    use({"catppuccin/nvim"})
    use({"RRethy/nvim-base16"})

    if packer_bootstrap then
        require("packer").sync()
    end

    plugin_config()
end)
