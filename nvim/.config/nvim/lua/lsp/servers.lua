return {
    init = function(on_attach)
        local capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())
        capabilities.textDocument.completion.completionItem.snippetSupport = false
        local lsp = require("lspconfig")
        local opts = { noremap=true, silent=true }

        lsp.serve_d.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.taplo.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.cmake.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.hls.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.zls.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.ols.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.rust_analyzer.setup {
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                ["rust-analyzer"] = {
                    assist = {
                        importGranularity = "module",
                        importPrefix = "by_self"
                    },
                    cargo = {
                        loadOutDirsFromCheck = true
                    },
                    procMacro = {
                        enable = true
                    }
                }
            }
        }

        lsp.nimls.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.bashls.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.clangd.setup {
            on_attach = function(c, b)
                on_attach(c, b)
                vim.api.nvim_buf_set_keymap(b, "n", "<Leader>p", "<cmd>ClangdSwitchSourceHeader<CR>", opts)
                vim.api.nvim_buf_set_keymap(b, "n", "<Leader>cb", "<cmd>CMake build<CR>", opts)
                vim.api.nvim_buf_set_keymap(b, "n", "<Leader>cr", "<cmd>CMake build_and_run<CR>", opts)
                vim.api.nvim_buf_set_keymap(b, "n", "<Leader>cc", "<cmd>CMake clear_cache<CR>", opts)
                vim.api.nvim_buf_set_keymap(b, "n", "<Leader>cg", "<cmd>CMake configure -G Ninja<CR>", opts)
            end,
            capabilities = capabilities,
        }

        lsp.solargraph.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        --[[ lsp.gdscript.setup {
            cmd = { "nc", "localhost", "6005" },
            on_attach = on_attach,
            capabilities = capabilities,
            flags = {
                debounce_text_changes = 150,
            }
        } ]]

        lsp.omnisharp.setup {
            handlers = {
                ["textDocument/definition"] = require("omnisharp_extended").handler,
            },
            on_attach = on_attach,
            capabilities = capabilities,
            cmd = {
                "omnisharp", "--languageserver", "--hostPID", tostring(vim.fn.getpid())
            }
        }

        lsp.pylsp.setup {
            on_attach = on_attach,
            capabilities = capabilities,
        }

        lsp.haxe_language_server.setup {
            on_attach = on_attach,
            capabilities = capabilities,
            cmd = {"node", "/home/minebill/git/haxe-language-server/bin/server.js"}
        }
    end
}
