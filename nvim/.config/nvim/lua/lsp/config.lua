local on_attach = function(client, bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }

	vim.diagnostic.config({
		float = {
			source = "always",
			border = border,
		},
	})

	vim.api.nvim_exec([[autocmd BufWritePost lua require("lint").try_lint()]], false)
	vim.keymap.set("n",	"gD", vim.lsp.buf.declaration, opts)
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
	vim.keymap.set("n", "gh", vim.lsp.buf.hover, opts)
	vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
	vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts)
	vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
	vim.keymap.set("n", "<leader>wl", vim.lsp.buf.list_workspace_folders, opts)
	vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, opts)
	vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
	vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, opts)
	vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
	vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, opts)
	vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
	vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
	vim.keymap.set("n", "<leader>ff", function() vim.lsp.buf.format({async = true}) end, opts)
	--[[ vim.keymap.set("n", "<leader>b", require'dap'.toggle_breakpoint(), opts)
	vim.keymap.set("n", "<leader>B", require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: ')), opts)
	vim.keymap.set("n", "<leader>ds", require'dap'.step_over(), opts)
	vim.keymap.set("n", "<leader>di", require'dap'.step_into(), opts)
	vim.keymap.set("n", "<leader>dc", require'dap'.continue(), opts) ]]
end

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
	virtual_text = true,
	signs = true,
	update_in_insert = true,
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
	border = "single",
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
	border = "single",
})

local servers = require("lsp/servers")
servers.init(on_attach)
