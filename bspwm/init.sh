#!/bin/bash
USE_COMPOSITOR=false

# Keyboard configuration
KEY_REPEAT_RATE_HZ=35
KEY_REPEAT_DELAY_MS=200
xset r rate $KEY_REPEAT_DELAY_MS $KEY_REPEAT_RATE_HZ
setxkbmap -layout us,gr -option grp:alt_caps_toggle

# Mouse configuration
xinput --set-prop "Logitech Gaming Mouse G502" 'libinput Accel Profile Enabled' 0, 1
xinput --set-prop "Logitech Gaming Mouse G502" 'libinput Accel Speed' -0.2

# Monitor configuration
xrandr --output DP-0 --mode 1920x1080 --rate 144
# xrandr --output HDMI2 --mode 1680x1050 --right-of DP-0

bash ~/.config/polybar/launch.sh

if command -v nitrogen &> /dev/null; then
    nitrogen --restore # Wallpaper manager
fi

if command -v lxsession &> /dev/null; then
   lxsession &
fi

if command -v dunst &> /dev/null; then
   dunst & # Notification daemon
fi

if command -v clipmenud &> /dev/null; then
    clipmenud & # Clipboard manager
fi

if command -v imwheel &> /dev/null; then
    imwheel &
fi
