# vim:ft=nix
# vim:sw=2
{ config, pkgs, ... }:

{
  imports =
    [
    ./hardware-configuration.nix
    ];
  nixpkgs.config.allowUnfree = true;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "pandora"; # Define your hostname.

  time.timeZone = "Europe/Athens";

  networking.useDHCP = false;
  networking.interfaces.enp5s0.useDHCP = true;

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  environment.variables = {
    EDITOR = "nvim";
  };

  services.xserver = {
    enable = true;
    windowManager = {
      bspwm.enable = true;
    };
    displayManager = {
      defaultSession = "xfce+bspwm";
      lightdm = {
        enable = true;
      };
    };
    desktopManager  = {
      xfce = {
        enable = true;
        enableXfwm = false;
      };
      wallpaper.mode = "fill";
    };
    autoRepeatDelay = 250;
    autoRepeatInterval = 35;
    videoDrivers = [ "nvidia" "intel" ];
    libinput = {
      enable = true;
      mouse = {
        accelProfile = "flat";
        accelSpeed = "0.3";
      };
    };
    # config = ''
    #   Section "InputClass"
    #     Identifier "mouse accel"
    #     Driver "libinput"
    #     MatchIsPointer "on"
    #     Option "AccelProfile" "0, 1"
    #     Option "AccelSpeed" "0.2"
    #   EndSection
    # '';
  };

  programs.ssh.startAgent = true;
  programs.steam.enable = true;

# Configure keymap in X11
# services.xserver.layout = "us";
# services.xserver.xkbOptions = "eurosign:e";
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };

# Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.minebill = {
    isNormalUser = true;
    initialPassword = "password";
    extraGroups = [ "wheel" ];
    shell = pkgs.zsh;
  };

  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    syntaxHighlighting = {
      enable = true;
    };
  };

  environment.systemPackages = with pkgs; [
    vim
      wget
      git
      alacritty
      firefox
      starship
      keepassxc
      zsh
  ];

# This value determines the NixOS release from which the default
# settings for stateful data, like file locations and database versions
# on your system were taken. It‘s perfectly fine and recommended to leave
# this value at the release version of the first install of this system.
# Before changing this value read the documentation for this option
# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
