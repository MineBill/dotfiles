from pynvim import attach
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--mode', type=str, required=True)
args = parser.parse_args()

colorscheme = 'catppuccin' if args.mode == 'light' else 'kanagawa' if args.mode == 'dark' else exit(1)

basedir = '/run/user/1000/'
for file in os.listdir(basedir):
    if file.startswith('nvim'):
        print(file)
        full_path = os.path.join(basedir, file)
        try:
            nvim = attach('socket', path=full_path)
            nvim.command('colorscheme ' + colorscheme)
            nvim.close()
        except ConnectionRefusedError:
            continue
