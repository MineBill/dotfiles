#!/bin/bash

selection=$(echo "Logout|Lock|Sleep|Restart|Shutdown" | rofi -sep "|" -dmenu -i -p 'Select action')

case $selection in
    Logout)
        bspc quit
        ;;
    Lock)
        ;;
    Sleep)
        systemctl suspend
        ;;
    Restart)
        systemctl reboot
        ;;
    Shutdown)
        systemctl poweroff
        ;;
    *)
        notify-send "Powermenu" "Invalid action '$selection'"
        ;;
esac
