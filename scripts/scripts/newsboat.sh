#!/bin/fish

set db_path "$HOME/.newsboat/cache.db"

while true;
    newsboat -x reload
    set unread (sqlite3 $db_path "select count(*) from rss_item where unread = 1")
    echo $unread
    sleep 30m
end
