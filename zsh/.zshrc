### Zsh Settings
SAVEHIST=1000
HISTFILE=~/.zsh_history
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
PROMPT="%F{125}[%f%B%F{red}%n%f%b%F{blue}::%f%F{green}%m%f%F{magenta} on %f%F{41}%~%f%F{magenta}]%f "

### Aliases
if type exa > /dev/null; then
    alias ls='exa'
fi
alias l='ls -l'
alias ll='ls -al'
alias cls=clear
alias hg='cat ~/.zsh_history | grep'
alias btw='neofetch'

### Exports
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export EDITOR="nvim"
export MANPAGER='nvim +Man!'
export MANWIDTH=999
export PATH=~/.local/bin/:$PATH
export PATH=~/.dotnet/tools/:$PATH
export GOPATH="$HOME/.go/"
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey '^H' backward-kill-word

### Plugin Settings
ZVM_VI_INSERT_ESCAPE_BINDKEY=fd
ZVM_CURSOR_STYLE_ENABLED=false

### Added by Zinit's installer
zi_home="${HOME}/.zi"
if [[ ! -d $zi_home ]]; then
    mkdir -p $zi_home
    git clone https://github.com/z-shell/zi.git "${zi_home}/bin"
fi

source "${zi_home}/bin/zi.zsh"
autoload -Uz _zi
(( ${+_comps} )) && _comps[zi]=_zi

### End of Zinit's installer chunk

### Plugins 
zi ice depth=1
zi light zsh-users/zsh-autosuggestions
zi light zsh-users/zsh-syntax-highlighting
