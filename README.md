# Personal repo for my dotfiles

`init/init.sh` is not executed when using a DE(eg. Gnome,KDE)

## Usage:
`stow --no-folding -vt ~ *` To stow everything or `stow --no-folding -vt ~ <folder>` to stow only the specified folder

## Needed software
- paru
- rofi
- polybar
- clipmenu

## Fonts used
- Hasklig

## List of software i use:
- paru:  
    AUR helper that's based on yay and written in rust.

## Nixos-Notes
Zsh is mainly configured from [home-manager](./home-manager/.config/nixpkgs/home.nix) but some of the configuration is in [zsh](./zsh)

Alacritty is also configured from [home-manager](./home-manager/.config/nixpkgs/home.nix) so for right now the alacritty folder is not
used.

## Newsboat
### Getting count of unread items:
One solution is using `newsboat -x print-unread` but that will fail when there is more than one instance of newsboat running.
Instead, a better solution is to simple run a query on the `~/.newsboat/cache.db`. For example: `select count(*) from rss_item where unread=1;`

## XMonad
XMonad is built using stack instead. Arch haskell packages are a mess.
