alias ls=exa
alias l='ls -l'
alias ll='ls -al'

alias cat='bat -n --theme OneHalfDark'
alias vim=nvim
alias cls=clear
alias d='cd ~/git/dotfiles'

alias gs='git status'
alias gp='git pull'
alias gap='git add -p'
alias gcm='git checkout master'

export EDITOR="nvim"
export MANPAGER="nvim +Man!"
export MANWIDTH=999
export FZF_DEFAULT_COMMAND="rg --follow --files"
export GODOT_MONO="~/.godot/Godot_v3.4.2-stable_mono_x11_64/Godot_v3.4.2-stable_mono_x11.64"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export NEOVIDE_MULTIGRID=1
export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia

export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$HOME/.emacs.d/bin/:$PATH"
export PATH="$HOME/.local/bin/:$PATH"
export PATH="$HOME/.cargo/bin/:$PATH"
export PATH="$HOME/.nimble/bin:$PATH"
export PATH="$HOME/code/scripts:$PATH"
export PATH="$HOME/git/zig/build/:$PATH"
export PATH="$HOME/git/zls/zig-out/bin:$PATH"
export PATH="$PATH:$GEM_HOME/bin"
export GOPATH="$HOME/.go/"
if command -v ruby &> /dev/null
    export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
end

set fish_greeting (echo Welcome)
if command -v starship &> /dev/null
    starship init fish | source
end
# vim:ft=fish
